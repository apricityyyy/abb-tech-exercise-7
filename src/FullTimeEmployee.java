public class FullTimeEmployee extends Employee {
    private final double salary;

    public FullTimeEmployee(String name, double experienceInYears, double salary) {
        super(name, experienceInYears);
        this.salary = salary;
    }

    @Override
    double getSalary() {
        return this.salary;
    }

    @Override
    public String toString() {
        return "FullTimeEmployee{" +
                "name=" + super.getName() +
                ", experience=" + super.getExperienceInYears() +
                ", salary=" + salary +
                '}';
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        long temp = Double.doubleToLongBits(salary);
        result = prime * result + (int) (temp);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true; // check references
        if (!super.equals(obj)) return false; // check superclass fields
        if (getClass() != obj.getClass()) return false; // check subclass equality

        FullTimeEmployee other = (FullTimeEmployee) obj; // compare additional fields
        return Double.compare(salary, other.salary) == 0;
    }
}
