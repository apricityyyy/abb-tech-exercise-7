public class PartTimeEmployee extends Employee {
    private final int hours;
    private final double hourSalary;

    public PartTimeEmployee(String name, double experienceInYears, int hours, double hourSalary) {
        super(name, experienceInYears);
        this.hours = hours;
        this.hourSalary = hourSalary;
    }

    @Override
    double getSalary() {
        return this.hours * this.hourSalary;
    }

    @Override
    public String toString() {
        return "PartTimeEmployee{" +
                "name=" + super.getName() +
                ", experience=" + super.getExperienceInYears() +
                ", hours=" + hours +
                ", hourSalary=" + hourSalary +
                '}';
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        long temp = Double.doubleToLongBits(this.getSalary());
        result = prime * result + (int) (temp);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true; // check references
        if (!super.equals(obj)) return false; // check superclass fields
        if (getClass() != obj.getClass()) return false; // check subclass equality

        PartTimeEmployee other = (PartTimeEmployee) obj; // compare additional fields
        return Double.compare(this.getSalary(), other.getSalary()) == 0;
    }
}
