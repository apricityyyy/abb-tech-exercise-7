import java.util.Objects;

public abstract class Employee implements Comparable<Employee> {
    private final String name;
    private final double experienceInYears;

    protected Employee(String name, double experienceInYears) {
        this.name = name;
        this.experienceInYears = experienceInYears;
    }

    abstract double getSalary();

    public String getName() {
        return name;
    }

    public double getExperienceInYears() {
        return experienceInYears;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true; // reference equality
        if (obj == null || getClass() != obj.getClass()) return false; // class equality
        Employee employee = (Employee) obj; // fields equality
        return experienceInYears == employee.experienceInYears &&
                (Objects.equals(name, employee.name));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp = Double.doubleToLongBits(experienceInYears);
        result = prime * result + (int) (temp);
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public int compareTo(Employee other) {
        return Double.compare(this.getSalary(), other.getSalary());
    }
}
