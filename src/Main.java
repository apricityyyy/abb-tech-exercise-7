import java.util.*;

public class Main {
    public static void main(String[] args) {
        // employees
        FullTimeEmployee e1 = new FullTimeEmployee("Rose", 1, 2000);
        FullTimeEmployee e2 = new FullTimeEmployee("Angela", 2.5, 5000);
        FullTimeEmployee e3 = new FullTimeEmployee("Bonnie", 3, 8000);

        PartTimeEmployee p1 = new PartTimeEmployee("Frank", 0.5, 40, 20);
        PartTimeEmployee p2 = new PartTimeEmployee("Lisa", 2, 60, 30);
        PartTimeEmployee p3 = new PartTimeEmployee("Michael", 4, 80, 50);

        ArrayList<Employee> employees = new ArrayList<>();
        employees.add(e1);
        employees.add(e2);
        employees.add(e3);
        employees.add(p1);
        employees.add(p2);
        employees.add(p3);

        // min and max salary employees on employees with experience more than 2 years
        Collections.sort(employees);

        LinkedList<Employee> sortedEmployees = new LinkedList<>();
        for (Employee e : employees) {
            if (e.getExperienceInYears() > 2) sortedEmployees.add(e);
        }

        System.out.println("Employee with maximum salary: " + sortedEmployees.getLast());
        System.out.println("Employee with minimum salary: " + sortedEmployees.getFirst());

        // mapping according to salaries
        Map<String, Employee> minMaxEmployees = new HashMap<>();
        minMaxEmployees.put("Minimum Salary", sortedEmployees.getFirst());
        minMaxEmployees.put("Maximum Salary", sortedEmployees.getLast());
        System.out.println(minMaxEmployees);
    }
}